package com.qing.quartz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuartzVue3Application {

    public static void main(String[] args) {
        SpringApplication.run(QuartzVue3Application.class, args);
    }

}
