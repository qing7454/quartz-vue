package com.qing.quartz.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "qrtz_job_details")
public class QuartzEntity implements Serializable {

    @Id
    @Column(name = "job_name", updatable = false, insertable = false)
    private String jobName;//任务名称
    @Column(name = "sched_name", updatable = false, insertable = false)
    private String schedName;//任务名称
    @Column(name = "job_group", updatable = false, insertable = false)
    private String jobGroup;//任务分组
    @Column(name = "description")
    private String description;//任务描述
    @Column(name = "job_Class_Name")
    private String jobClassName;//执行类
    @Column(name = "is_durable")
    private String durable;//执行类
    @Column(name = "is_nonconcurrent")
    private String nonconcurrent;//执行类
    @Column(name = "is_update_data")
    private String updateData;//执行类
    @Column(name = "requests_recovery")
    private String requestsRecovery;//执行类
    @Column(name = "job_data")
    private String jobData;//执行类

    @Transient
    private String jobMethodName;//执行方法
    @Transient
    private String cronExpression;//执行时间
    @Transient
    private String triggerName;//执行时间
    @Transient
    private String triggerState;//任务状态

    @Transient
    private String oldJobName;//任务名称 用于修改
    @Transient
    private String oldJobGroup;//任务分组 用于修改
}
