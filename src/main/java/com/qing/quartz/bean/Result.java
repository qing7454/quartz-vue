package com.qing.quartz.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {

    private static final long serialVersionUID = 5576237395711742681L;

    public static final int SUCCESS = 1;
    public static final int FAILURE = -1;

    private boolean success = false;
    private String msg = "";
    private Object obj = null;

    public static final String MSG_SUCCESS = "操作成功";
    public static final String MSG_ERROR = "操作失败";

    public static final boolean STATUS_SUCCESS = true;
    public static final boolean STATUS_ERROR = false;


    public Result() {
    }

    public Result(Boolean success, String msg) {
        this.success = success;
        this.msg = msg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }


    public static  Result success(String msg, Object obj) {
        Result result = success(msg);
        result.setObj(obj);
        return result;
    }

    public static  Result success(String msg) {
        Result result = success();
        result.setMsg(msg);
        return  result;
    }
    public static  Result success(Object msg) {
        Result result = success();
        result.setObj(msg);
        return  result;
    }

    public static  Result success() {
        return new Result(STATUS_SUCCESS, MSG_SUCCESS);
    }

    public static  Result error(String msg, Object obj) {
        Result result = error(msg);
        result.setObj(obj);
        return result;
    }

    public static  Result error(String msg) {
        Result result = error();
        result.setMsg(msg);
        return  result;
    }
    public static Result error(Object msg) {
        Result result = error();
        result.setObj(msg);
        return  result;
    }

    public static Result error() {
        return new Result(STATUS_ERROR, MSG_ERROR);
    }

    @Override
    public String toString() {
        return "Result{" +
                "success=" + success +
                ", msg='" + msg + '\'' +
                ", obj=" + obj +
                '}';
    }
}