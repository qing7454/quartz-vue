package com.qing.quartz.bean;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PageInfo<T> {

    private long total;
    private List<T> data;
    private int page;
    private int size;
}
