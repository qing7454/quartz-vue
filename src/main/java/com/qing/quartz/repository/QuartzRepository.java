package com.qing.quartz.repository;

import com.qing.quartz.entity.QuartzEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Map;

public interface QuartzRepository extends JpaRepository<QuartzEntity, Integer> {

    @Query(value = "SELECT job.JOB_NAME as jobName,job.JOB_GROUP as jobGroup," +
            "job.DESCRIPTION as description,job.JOB_CLASS_NAME as jobClassName," +
            "cron.CRON_EXPRESSION as cronExpression,tri.TRIGGER_NAME as triggerName," +
            "tri.TRIGGER_STATE as triggerState,job.JOB_NAME as oldJobName," +
            "job.JOB_GROUP as oldJobGroup " +
            "FROM qrtz_job_details AS job " +
            "LEFT JOIN qrtz_triggers AS tri ON job.JOB_NAME = tri.JOB_NAME  AND job.JOB_GROUP = tri.JOB_GROUP " +
            "LEFT JOIN qrtz_cron_triggers AS cron ON cron.TRIGGER_NAME = tri.TRIGGER_NAME AND cron.TRIGGER_GROUP= tri.JOB_GROUP " +
            "WHERE tri.TRIGGER_TYPE = 'CRON' " +
            " ",
            countQuery = "SELECT count(*) " +
                    "FROM qrtz_job_details AS job " +
                    "LEFT JOIN qrtz_triggers AS tri ON job.JOB_NAME = tri.JOB_NAME  AND job.JOB_GROUP = tri.JOB_GROUP " +
                    "LEFT JOIN qrtz_cron_triggers AS cron ON cron.TRIGGER_NAME = tri.TRIGGER_NAME AND cron.TRIGGER_GROUP= tri.JOB_GROUP " +
                    "WHERE tri.TRIGGER_TYPE = 'CRON' " +
                    " ",
            nativeQuery = true)
    Page<Map<String, Object>> queryListModel(@Param(value = "jobName") String jobName, Pageable pageable);

    @Query(value = "SELECT COUNT(*) FROM qrtz_job_details AS job " +
            "LEFT JOIN qrtz_triggers AS tri ON job.JOB_NAME = tri.JOB_NAME " +
            "LEFT JOIN qrtz_cron_triggers AS cron ON cron.TRIGGER_NAME = tri.TRIGGER_NAME " +
            "WHERE tri.TRIGGER_TYPE = 'CRON'", nativeQuery = true)
    Long queryCount();
}
