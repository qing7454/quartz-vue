package com.qing.quartz.query;

import lombok.Data;

import java.io.Serializable;

@Data
public class TaskQuery implements Serializable {

    private Integer page;
    private Integer size;
    private String jobName;
}
