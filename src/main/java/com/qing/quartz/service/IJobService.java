package com.qing.quartz.service;

import com.qing.quartz.bean.PageInfo;
import com.qing.quartz.entity.QuartzEntity;
import com.qing.quartz.query.TaskQuery;
import org.quartz.SchedulerException;

import java.util.Map;


public interface IJobService {

    PageInfo<Map<String, Object>> listQuartzEntity(TaskQuery taskQuery) throws SchedulerException;

    Long listQuartzEntity(QuartzEntity quartz);
}
