package com.qing.quartz.service.impl;

import com.qing.quartz.bean.PageInfo;
import com.qing.quartz.entity.QuartzEntity;
import com.qing.quartz.query.TaskQuery;
import com.qing.quartz.repository.QuartzRepository;
import com.qing.quartz.service.IJobService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("jobService")
@Slf4j
public class JobServiceImpl implements IJobService {

    @Autowired @Qualifier("Scheduler")
    private Scheduler scheduler;
    @Autowired
    private QuartzRepository quartzRepository;

	@Override
	public PageInfo<Map<String, Object>> listQuartzEntity(TaskQuery taskQuery) throws SchedulerException {
		Pageable pageable = PageRequest.of(taskQuery.getPage() - 1, taskQuery.getSize());
        Page<Map<String, Object>> list = quartzRepository.queryListModel(taskQuery.getJobName(), pageable);
        List<Map<String, Object>> data = new ArrayList<>();
        for (Map<String, Object> quartzEntity : list) {
            JobKey key = new JobKey(quartzEntity.get("jobName")+"", quartzEntity.get("jobGroup")+"");
            JobDetail jobDetail = scheduler.getJobDetail(key);
            Map<String, Object> newMap = new HashMap<>(quartzEntity);
            log.info("key: {}, jobDetail: {}", key, jobDetail);
            if (null != jobDetail)
            newMap.put("jobMethodName", jobDetail.getJobDataMap().getString("jobMethodName"));
            data.add(newMap);
        }
        return PageInfo.<Map<String, Object>>builder().data(data).total(list.getTotalElements())
                .page(taskQuery.getPage()).size(taskQuery.getSize()).build();
	}

	@Override
	public Long listQuartzEntity(QuartzEntity quartz) {
		return quartzRepository.queryCount();
	}
}
