package com.qing.quartz.conrtoller;

import com.qing.quartz.bean.PageInfo;
import com.qing.quartz.bean.Result;
import com.qing.quartz.entity.QuartzEntity;
import com.qing.quartz.query.TaskQuery;
import com.qing.quartz.service.IJobService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/task")
@Slf4j
public class TaskController {

    @Autowired
    private IJobService jobService;
    @Autowired
    @Qualifier("Scheduler")
    private Scheduler scheduler;

    /**
     * 查询任务列表
     * @param taskQuery 查询条件
     * @return data
     * @throws SchedulerException e
     */
    @PostMapping(value = "/list")
    public PageInfo<Map<String, Object>> taskList(@RequestBody TaskQuery taskQuery) throws SchedulerException {
        return jobService.listQuartzEntity(taskQuery);
    }

    /**
     * 新建定时任务
     * @param quartz 数据
     * @return result
     */
    @PostMapping("/create")
    public Result save(@RequestBody QuartzEntity quartz) {
        log.info("新增任务");
        try {
            //获取Scheduler实例、废弃、使用自动注入的scheduler、否则spring的service将无法注入
            //Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            //如果是修改  展示旧的 任务
            if (quartz.getOldJobGroup() != null) {
                JobKey key = new JobKey(quartz.getOldJobName(), quartz.getOldJobGroup());
                scheduler.deleteJob(key);
            }
            Class cls = Class.forName(quartz.getJobClassName());
            cls.newInstance();
            //构建job信息
            JobDetail job = JobBuilder.newJob(cls).withIdentity(quartz.getJobName(),
                    quartz.getJobGroup())
                    .withDescription(quartz.getDescription()).build();
            job.getJobDataMap().put("jobMethodName", quartz.getJobMethodName());
            // 触发时间点
            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(quartz.getCronExpression());
            Trigger trigger = TriggerBuilder.newTrigger().withIdentity("trigger" + quartz.getJobName(), quartz.getJobGroup())
                    .startNow().withSchedule(cronScheduleBuilder).build();
            //交由Scheduler安排触发
            scheduler.scheduleJob(job, trigger);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 执行一次任务，仅执行一次
     * @param quartz 数据
     * @return result
     */
    @PostMapping(value = "/triggerTask")
    public Result triggerTask(@RequestBody QuartzEntity quartz) {
        log.info("执行一次任务");
        try {
            JobKey key = new JobKey(quartz.getJobName(), quartz.getJobGroup());
            scheduler.triggerJob(key);
        } catch (SchedulerException e) {
            e.printStackTrace();
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 暂停任务
     * @param quartz 数据
     * @return result
     */
    @PostMapping(value = "/pauseTask")
    public Result pauseTask(@RequestBody QuartzEntity quartz) {
        log.info("暂停任务");
        try {
            JobKey key = new JobKey(quartz.getJobName(), quartz.getJobGroup());
            scheduler.pauseJob(key);
        } catch (SchedulerException e) {
            e.printStackTrace();
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 开启一个任务
     * @param quartz 开启的任务数据
     * @return result
     */
    @PostMapping(value = "/resumeTask")
    public Result resumeTask(@RequestBody QuartzEntity quartz) {
        log.info("开启任务");
        try {
            JobKey key = new JobKey(quartz.getJobName(), quartz.getJobGroup());
            scheduler.resumeJob(key);
        } catch (SchedulerException e) {
            e.printStackTrace();
            return Result.error();
        }
        return Result.success();
    }

    /**
     * 移除调度器中的任务
     * @param quartzEntity 要删除的数据
     * @return result
     */
    @PostMapping(value = "/removeTask")
    public Result removeTask(@RequestBody QuartzEntity quartzEntity) {
        try {
            //获取调度的key
            TriggerKey triggerKey = TriggerKey.triggerKey(quartzEntity.getJobName(), quartzEntity.getJobGroup());
            //先停止任务
            scheduler.pauseTrigger(triggerKey);
            //一处出发器
            scheduler.unscheduleJob(triggerKey);
            //再删除掉触发器，避免出现执行错误
            JobKey jobKey = new JobKey(quartzEntity.getJobName(), quartzEntity.getJobGroup());
            scheduler.deleteJob(jobKey);
            log.info("移除任务：jobKey: {}, quartz: {}", jobKey, quartzEntity);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error();
        }
        return Result.success();
    }

}
